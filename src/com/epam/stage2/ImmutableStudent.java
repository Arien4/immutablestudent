package com.epam.stage2;

import java.util.Objects;

public class ImmutableStudent {
    private final String name;
    private final Age age;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImmutableStudent)) return false;
        ImmutableStudent that = (ImmutableStudent) o;
        return name.equals(that.name) && Objects.equals(age, that.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    public ImmutableStudent(String name, Age age) {
        this.name = name;
        Age cloneAge = new Age();
        cloneAge.setYear(age.getYear());
        this.age = cloneAge;
    }

    public String getName() {
        return name;
    }

    public Age getAge() {
        Age cloneAge = new Age();
        cloneAge.setYear(this.age.getYear());
        return cloneAge;
    }

    public ImmutableStudent setName(String name) {
        return new ImmutableStudent(name, this.age);
    }

    public ImmutableStudent setAge(Age age) {
        return new ImmutableStudent(this.name, age);
    }
}
